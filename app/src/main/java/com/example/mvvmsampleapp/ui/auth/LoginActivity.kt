package com.example.mvvmsampleapp.ui.auth

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.mvvmsampleapp.R.layout.activity_login
import com.example.mvvmsampleapp.data.db.entities.User
import com.example.mvvmsampleapp.databinding.ActivityLoginBinding
import com.example.mvvmsampleapp.util.hide
import com.example.mvvmsampleapp.util.show
import com.example.mvvmsampleapp.util.toast
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), AuthListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //step 1: binding
        val binding: ActivityLoginBinding =
            DataBindingUtil.setContentView(this, activity_login)
        //step 2 : get viewModel
        val viewModel = ViewModelProviders.of(this).get(AuthViewModel::class.java)
        //step 3 : set viewModel in xml as our binding viewModel class
        //now it will bind our data whit the UI
        binding.viewmodel = viewModel

        //step 4 : define the AuthListener
        viewModel.authListener = this

    }

    override fun onStarted() {
        progress_bar.show()
    }

    override fun onSuccess(user: User) {
        progress_bar.hide()
        toast("${user.name} is Logged In")
    }

    override fun onFailure(message: String) {
        progress_bar.hide()
        toast(message)
    }
}