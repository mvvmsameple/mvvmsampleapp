package com.example.mvvmsampleapp.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.mvvmsampleapp.data.db.entities.User

@Database(
    entities = [User::class],
    version = 1
)

//whenever creating a database class for Room you need to make it abstract and need to extend RoomDatabase
abstract class AppDatabase : RoomDatabase() {
    abstract fun getUserDao(): UserDao


    companion object {
        //Volatile meant this variable is immediately visible to all the other threads.
        @Volatile
        private var instance: AppDatabase? = null
        private val LOCk = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCk) {
            instance ?: buildDatabase(context).also {
                instance = it
            }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                "MyDatabase.db"
            ).build()
    }

}