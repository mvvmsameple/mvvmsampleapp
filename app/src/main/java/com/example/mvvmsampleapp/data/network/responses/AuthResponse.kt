package com.example.mvvmsampleapp.data.network.responses

import com.example.mvvmsampleapp.data.db.entities.User

//this class will do nothing, it will just store the JSON response.
//this class directly parse our JSON response to Kotlin classes. Or can say Kotlin object
data class AuthResponse(
    val isSuccessful: Boolean?,
    val message: String?,
    val user: User?
)