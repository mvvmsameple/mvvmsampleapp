package com.example.mvvmsampleapp.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

//do not make the ID auto increment, because we don't want to store multiple user for now
const val CURRENT_USER_ID = 0

//if you want to chang table name in room database you can define a table name in @Entity(tableName = "name")
@Entity()
data class User(
    var id: Int? = null,
    var name: String? = null,
    var email: String? = null,
    var email_verified_at: String? = null,
    var created_at: String? = null,
    var updated_at: String? = null
){
    @PrimaryKey(autoGenerate = false)
    var uid: Int = CURRENT_USER_ID
}